FROM nginx:1.16.1-alpine

COPY nginx.conf /etc/nginx/nginx.conf

COPY ./dist/quiz /usr/share/nginx/html

RUN addgroup -S runner -g 433 && \
    adduser -u 431 -S -g runner -h /opt -s /sbin/nologin runner

RUN chown -R runner:runner /var/cache/nginx
RUN chown -R runner:runner /etc/nginx
RUN chown -R runner:runner /usr/share/nginx
RUN chown -R runner:runner /var/run

EXPOSE 8080 8080

USER runner